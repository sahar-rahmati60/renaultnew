const gulp = require("gulp");
const sass = require("gulp-sass");
const concat = require("gulp-concat");
const rename = require("gulp-rename");
const uglify = require("gulp-uglify");
const babel = require("gulp-babel");
const browserSync = require("browser-sync").create();

const jsSRC = [
  "node_modules/jquery/dist/jquery.min.js",
  "node_modules/bootstrap/dist/js/bootstrap.min.js",
  "./js/**/*.js",
];

function style() {
  return gulp
    .src("./scss/**/*.scss")
    .pipe(sass({ includePaths: ["node_modules"] }).on("error", sass.logError))
    .pipe(gulp.dest("./dist/css"))
    .pipe(browserSync.stream());
}

function js() {
  return gulp
    .src(jsSRC)
    .pipe(concat("scripts.js"))
    .pipe(gulp.dest("./dist/js"))
    .pipe(
      babel({
        presets: ["@babel/env"],
      })
    )
    .pipe(uglify())
    .pipe(rename("scripts.min.js"))
    .pipe(gulp.dest("./dist/js"));
}

function watch() {
  browserSync.init({
    server: {
      baseDir: "./",
    },
  });
  gulp.watch("./scss/**/*.scss", style);
  gulp.watch("./*.html").on("change", browserSync.reload);
  gulp.watch(jsSRC, js);
}
exports.style = style;
exports.js = js;
exports.watch = watch;
