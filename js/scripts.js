$("document").ready(function ($) {
  var nav = $("#nav-header");
  var $nav = $(".navi a");
  var shareBtn1 = $(".shareBtn");
  var socialShare = $(".social-share");
  var continueLink = $(".continueLink a");

  $nav.click(function (e) {
    e.preventDefault();
    $("html, body").animate(
      {
        scrollTop: $($(this).attr("href")).offset().top - 75,
      },
      1000
    );
  });

  continueLink.click(function (e) {
    e.preventDefault();
    $("html, body").animate(
      {
        scrollTop: $($(this).attr("href")).offset().top - 180,
      },
      1000
    );
  });

  $(window).scroll(function () {
    if ($(this).scrollTop() > 125) {
      nav.addClass("fixed-nav");
    } else {
      nav.removeClass("fixed-nav");
    }
  });

  shareBtn1.click(function (e) {
    socialShare.fadeToggle()("social-share");
  });
});

function validateForm() {
  var familyInput = document.getElementById("family");
  var mobileInput = document.getElementById("mobile");
  var vinInput = document.getElementById("vin");

  var errorFamily = document.querySelector(".error-family");
  var errorMobile = document.querySelector(".error-mobile");
  var errorVIN = document.querySelector(".error-vin");

  var submit = true;
  var text;
  if (familyInput.value == "" || familyInput.value == null) {
    text = "وارد کردن نام و نام خانوادگی اجباری است";
    errorFamily.innerHTML = text;
    submit = false;
  } else {
    errorFamily.innerHTML = "";
  }
  if (mobileInput.value == "" || mobileInput.value == null) {
    text = "وارد کردن شماره همراه اجباری است";
    errorMobile.innerHTML = text;
    submit = false;
  } else {
    errorMobile.innerHTML = "";
  }
  if (vinInput.value.length < 8 || vinInput.value == "") {
    text = "ٰحداقل تعداد کاراکتر هشت می باشد";
    errorVIN.innerHTML = text;
    submit = false;
  } else {
    errorVIN.innerHTML = "";
  }
  return submit;
}

function validateSecurity() {
  var codeInput = document.getElementById("code");
  var errorCode = document.querySelector(".error-code");

  var submitCode = true;
  var textError;
  var x = "test";

  if (codeInput.value !== x) {
    textError = "ٰکد امنیتی صحیح وارد نشده است";
    errorCode.innerHTML = textError;
    submitCode = false;
  } else {
    errorCode.innerHTML = "";
  }
  return submitCode;
}

function validate() {
  if (validateForm()) {
    var el1 = document.querySelector(".register-main");
    var el2 = document.querySelector(".security-code");
    el1.style.display = "none";
    el2.style.display = "block";
  }
}

function validateFormSecurity() {
  if (validateSecurity()) {
    var el3 = document.querySelector(".security-code");
    var el4 = document.querySelector(".succes-register");
    el3.style.display = "none";
    el4.style.display = "block";
  }
}

$(window).on("load resize", function () {
  $(".height-js").height($(this).height());
});

$("document").ready(function ($) {
  var btn = $(".vin-guide-btn");
  var guidTetx = $(".vin-guide");
  var closeGuide = $(".close-guide");

  btn.click(function (e) {
    e.preventDefault();
    guidTetx.fadeIn();
  });
  closeGuide.click(function (e) {
    e.preventDefault();
    guidTetx.fadeOut();
  });
});

const shareButton = document.querySelector(".share-button");
const shareDialog = document.querySelector(".share-dialog");
const closeButton = document.querySelector(".close-button");

shareButton.addEventListener("click", (event) => {
  var vinCode = "";
  if (navigator.share) {
    navigator
      .share({
        title:
          "در کمپین تابستانی رنویار شرکت کنید و کد معرفی را با دوستان خود به اشتراک بگذارید" +
          "" +
          vinCode,
        url: "https://renaultyar.com/campaign/renaultyarsummer/",
      })
      .then(() => {
        console.log("Thanks for sharing!");
      })
      .catch(console.error);
  } else {
    shareDialog.classList.add("is-open");
  }
});

closeButton.addEventListener("click", (event) => {
  shareDialog.classList.remove("is-open");
});

jQuery("#modalVideo").on("hidden.bs.modal", function (e) {
  $("#ytplayer").attr("src", "");
});
jQuery("#modalVideo").on("show.bs.modal", function (e) {
  $("#ytplayer").attr("src", "../video/video.mp4");
});
